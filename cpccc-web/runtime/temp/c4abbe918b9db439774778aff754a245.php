<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:87:"/www/wwwroot/im.makerjie.com/cpccc-web/public/../application/admin/view/user/index.html";i:1570879002;s:80:"/www/wwwroot/im.makerjie.com/cpccc-web/application/admin/view/public/header.html";i:1571310830;s:80:"/www/wwwroot/im.makerjie.com/cpccc-web/application/admin/view/public/footer.html";i:1570879000;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0"/>
	<title>哈土豆IM管理后台</title>
	<link rel="stylesheet" type="text/css" href="/static/admin/libs/layui/css/layui.css">
	<script src="/static/admin/js/jquery-1.12.4.min.js"></script>
	<script src="/static/admin/libs/layui/layui.js"></script>
	<script src="/static/admin/js/main.js?v=1.0"></script>
	<link rel="stylesheet" type="text/css" href="/static/admin/css/main.css?v=OoO4pQ==">
</head>

<body>
<div class="layui-header">
	<div class="logo">哈土豆</div>
	<ul class="layui-nav" lay-filter="">
		<li class="layui-nav-item"><a href="javascript:void(0);" onclick="account_edit()">改密码</a></li>
		<li class="layui-nav-item"><a href="/admin/login/logout">退出</a></li>
	</ul>
</div>

<ul class="layui-nav layui-nav-tree" lay-filter="test">
	<ul class="layui-nav layui-nav-tree layui-nav-side">
		<li class="layui-nav-item <?php if($section=="user"): ?>layui-this<?php endif; ?>"><a href="/admin/user/index">用户列表</a></li>
		<li class="layui-nav-item <?php if($section=="group"): ?>layui-this<?php endif; ?>"><a href="/admin/group/index">群组列表</a></li>
		<li class="layui-nav-item <?php if($section=="message"): ?>layui-this<?php endif; ?>""><a href="/admin/message/index">消息监控</a></li>
		<li class="layui-nav-item <?php if($section=="common"): ?>layui-this<?php endif; ?>"><a href="/admin/common/index">功能设置</a></li>
	</ul>
</ul>
<div class="main-body">

</div>
<div class="main-body">
	<table class="layui-table" lay-even lay-skin="line">
		<colgroup>
			<col width="150">
			<col width="200">
			<col>
		</colgroup>
		<thead>
		<tr class="table_head_background">
			<th width="3%">uid</th>
			<th width="3%">头像</th>
			<th width="5%">登录名</th>
			<th width="10%">昵称</th>
			<th width="15%">注册时间</th>
			<th width="15%">操作</th>
		</tr>
		</thead>
		<tbody>
		<?php if(is_array($users) || $users instanceof \think\Collection || $users instanceof \think\Paginator): $i = 0; $__LIST__ = $users;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
		<tr>
			<td><?php echo $item['uid']; ?></td>
			<td><img src="<?php echo $item['avatar']; ?>" width="40px"/></td>
			<td><?php echo $item['username']; ?></td>
			<td><?php echo $item['nickname']; ?></td>
			<td><?php echo date("Y-m-d H:i:s",$item['timestamp']); ?></td>
			<td uid="<?php echo $item['uid']; ?>">
				<button class="layui-btn layui-btn-sm" pop-event="edit_user">编辑</button>
				<button class="layui-btn layui-btn-sm" pop-event="message_log">聊天日志</button>
				<?php if($item['account_state']=='disabled'): ?>
				    <button class="layui-btn layui-btn-sm layui-btn-warm" pop-event="enable_user">启用</button>
				<?php else: ?>
				    <button class="layui-btn layui-btn-sm layui-btn-danger" pop-event="disable_user">禁用</button>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; endif; else: echo "" ;endif; ?>
		</tbody>
	</table>

	<div id="page"></div>


	<script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage;
            laypage.render({
                elem: 'page'
				, limit: <?php echo $step; ?>
                , count: <?php echo $count; ?> //数据总数，从服务端得到
				, curr: <?php echo $page; ?>
                , jump: function (obj, first) {
                    //首次不执行
                    if (!first) {
                        //do something
                        location = '/admin/user/index?page='+obj.curr;
                    }
                }
            });

            $('.main-body').on("click", "*[pop-event]", function (e) {
                var event = $(this).attr('pop-event');
                callback[event]($(e.target.parentNode).attr('uid'));
            });

            var callback = {
                edit_user: function (uid) {
                    layer.open({
                        type: 2,
                        title: '编辑用户信息',
                        content: '/admin/user/edit?uid=' + uid,
                        area: ['800px', '550px']
                    });
                },
                message_log:function (uid) {
                    layer.open({
                        type: 2,
						title: '聊天日志',
                        content: '/admin/user/chatlog?uid=' + uid,
                        area: ['800px', '550px']
                    });
                },
                enable_user:function (uid) {
                    $.ajax({
                        url:'/admin/user/update',
                        type:'post',
                        data: {uid:uid, account_state:'normal'},
                        success:function(res){
                            if (res.code == 0) {
                                layui.layer.msg('操作成功');
                                setTimeout(function(){location.reload();}, 1000);
                            } else {
                                layui.layer.msg(res.msg);
                            }
                        }
                    });
                },
                disable_user:function (uid) {
                    layer.confirm('确定禁用该用户么？', {icon: 3, title:'提示'}, function(index) {
                        $.ajax({
                            url: '/admin/user/update',
                            type: 'post',
                            data: {uid: uid, account_state: 'disabled'},
                            success: function (res) {
                                if (res.code == 0) {
                                    layui.layer.msg('操作成功');
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    layui.layer.msg(res.msg);
                                }
                            }
                        });
                    });
                }
            }
        });

	</script>
</div>


</body>
</html>


