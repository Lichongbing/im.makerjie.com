<?php
namespace app\member\controller;
use think\Db;

class Money extends Base{
	
	public function _initialize(){
        parent::_initialize();
        $this->assign('getMoneyTypes',model('Users')->getMoneyTypes());
		$this->assign('getPrestigeTypes',model('Users')->getPrestigeTypes());
		$this->assign('getIntegralTypes',model('Users')->getIntegralTypes());
    }
 
	
	public function index(){
		$this->assign('money', $money = Db::name('payment_logs')->where(array('user_id'=>$this->uid,'is_paid'=>1))->sum('need_pay'));
		$map = array('user_id' => $this->uid);
        $count = Db::name('user_money_logs')->where($map)->count();
        $Page = new \Page($count, 16);
        $show = $Page->show();
        $list = Db::name('user_money_logs')->where($map)->order(array('log_id' => 'desc'))->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('list', $list);
        $this->assign('page', $show);
        return $this->fetch();
	}
	
    public function money(){
        $this->assign('payment', model('Payment')->getPayments());
        return $this->fetch();
	}
   
    public function moneypay(){
        $money = (int)(input('money') * 100);
        $code = input('code', 'htmlspecialchars');
        if($money <= 0){
			 return json(array('code' => '0', 'msg' => '请填写正确的充值金额！'));
        }
        $payment = model('Payment')->checkPayment($code);
        if(empty($payment)){
			return json(array('code' => '0', 'msg' => '支付方式不存在'));
        }
		
        $logs = array(
			'user_id' => $this->uid, 
			'type' => 'money', 
			'code' => $code, 
			'order_id' => 0, 
			'need_pay' => $money, 
			'create_time' => time(), 
			'create_ip' => request()->ip()
		);
        $logs['log_id'] = Db::name('payment_logs')->insertGetId($logs);
		
		
        $this->assign('button', model('Payment')->getCode('',$logs));
		$this->assign('paytype', model('Payment')->getPayments());
        $this->assign('money', $money);
		$this->assign('log_id', $logs['log_id']);
		return json(array('code' => '1', 'msg' => '正在为您跳转到支付',url =>url('Home/payment/payment', array('log_id' => $logs['log_id']))));
	}
   
 
	
      //检测扫码支付支付状态
	 public function check(){
		$log_id = input('log_id');
        $paymentlogs = Db::name('payment_logs')->find($log_id);
        if (!empty($paymentlogs) && $paymentlogs['is_paid'] ==1){
          return json(array('status' => 'success', 'msg' => '恭喜您支付成功，正在为您跳转'));
        }
	 }
	 
	 
}